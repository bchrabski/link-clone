# SmarterWidgets

**SmarterWidgets** is a set of extensions built for the needs of IBM Engineering platform users (DOORS Next, Test Management, Global Configuration), which are intended to facilitate everyday work or more complex work requiring automation.

The set of extensions is still being developed so the list of available extensions will be constantly growing



![](.\images\reqpro.png)



## Table of Contents

[TOC]


---------------------------------------

## List of widgets

1. **TermMap** - Create references between glossary terms and artefacts.  

2. **Terms Remove** - The purpose of this widget is to remove links between the definitions (glossary terms) used and the artifact.

3. **Broken Links - Primary Text** - The widget is searching for artifacts with broken links in the primary text.

4. **Auto Number Sort Attribute** - The widget is automatically updating integer values of specified sort attribute.

5. **Compare** - Widget to compare artifact primary text with other string based attribute.

6. **Cyber Security** - Use widget to calculate values of Secure Risk Rating. Calculations are done for artifact type Threat Scenario.

7. **Safety Risk Management** - Use widget to calculate values of Probability of Occurrence of Harm and Risk

8. **Link Type Clone** - The widget clones all selected link types into another link type. 

9. **Link Type Delete** - The widget deletes all links of selected link type.

10. **Style Clean** - The widget is finding removing HTML styles from artifact content.

11. **Primary Text Copy To Attribute** - Widget copies value from Primary Text (as plain text) to user-specified Destination Attribute.

12. **Module Fill** - The module fill widget is creating a module structure based on the pairs of artifact identifiers and level in module structure. 

13. **Quality Assurance**  - This extension allows you to check quality of selected artifacts based on the defined rules.

15. **D&R Rule Checker** - Widget checks completeness of attributes and D&R rule compliance for selected artifacts (i.e., requirements, parameters, and/or terms). 

13. **IBM ASIL Guidance** - This extension checks modules that include information related to the ISO 26262 ASIL standard.

14. **IBM Attributes and Links** - This extension shows the attributes and links of the artifact that is currently selected.

15. **IBM Import Repair** - This extension allows you to take module with a flat hierarchy but an attribute on the artifacts that indicates depth and move all the artifacts in it to the corresponding depth.

16. **IBM Module Explorer** - This extension shows a hierarchical tree view of the module, which you can click to explore.

17. **IBM Split and Join** - This extension operates on Text artifacts in a Module and and either  join a  group artifacts into a single artifact or split a single  artifact with multiple paragraphs into individual  artifacts in the Module.

    


---------------------------------------

### TermMap

Create references between glossary terms and artefacts.

![TermMap](.\images\termap_full.png)

**Version:**

Version 4 (release date 15.2.2021)

**Description:**

Many customers working with DOORS Next face a challenge where they have requirements documents in the product and hundreds or thousands of definitions in the dictionaries. Linking these elements together as a relationship in a manual way will be a process that will take a lot of time and may lead to errors.

The IBM DOORS Next product widget automatically retrieves a list of available definitions from the project and then creates a relationship between the artifact and the definition by creating a link and hyperlink in the text.

The widget allows to create references for requirements selected by the user or to do this work for the whole module with requirements.

**Use:**

***Step 1:***

In step 1 user is asked to define artifact type of glossary term (default: Term) in the widget properties.



![](.\images\termap_1.png)



![](.\images\termap_2.png)



Next user should select terms in the project that will be linked to the artefact later or can user can use option to find all glossary terms in the project that will return complete list of terms.



![](.\images\termap_3.png)

***Step 2:***

**Simple Mode**

Link selected artifact(s) to the glossary terms from step 1.



![](.\images\termap_4.png)



**Module Mode**

Link all artifact(s) within entire module to the glossary terms from step 1. Mode is enabled only when module is open. In this mode the reference links are created in base context.



![](.\images\termap_5.png)

---------------------------------------

### Terms Remove

The purpose of this widget is to remove links between the definitions (glossary terms) used and the artifact.



![Terms Remove](.\images\termsRemove_full.png)

**Version:**

Version 1 (release date 15.2.2021)

**Description:**

Widget was created to remove unwanted references to definitions, terms, synonyms. The widget removes both references from the artifact text and links to other artifacts.



**Artifact before running the widget.**

![](.\images\termsRemove_2.png)



**Artifact after running the widget**

![](.\images\termsRemove_3.png)



**Use:**

The widget works in two modes: for user selected artifacts and for entire modules. When running for whole modules, it is recommended to run the widget in change set in order not to delete links by running the widget incorrectly.



**Simple Mode:**

In this mode, select the artifacts which you want to remove links and hyperlinks to definitions and click the "Remove terms from all selected artifacts" button.

![](.\images\termsRemove_1.png)



**Module Mode:**

In this mode, the widget goes through all artifacts in the module and removes hyperlinks from the text and links to definitions, terms, synonyms, and objects marked as glossary type.

![](.\images\termsRemove_4.png)




---------------------------------------

### Broken Links - Primary Text

This extension checks modules that include information related to the ISO 26262 ASIL standard.


![Broken Links - Primary Text](.\images\brokenlinks_full.png)

**Version:**

Version 2.0 (release date 04.03.2022)

**Description:**

The purpose of the widget is to search the primary text artifacts in DOORS Next for hyperlinks that lead to other artifacts in DOORS that may be deleted which may lead to broken hyperlinks.

The widget displays a list of artifacts that have broken links in primary text.

The widget may be extended in the future to check for broken links in the links section.

**Use:**

Widget is only working when module is open the DOORS Next, outside the module "Find artifacts with broken links in primary text" button will be disabled.

![](.\images\brokenlinks_1.png)



After opening the module the widget is ready for use.



![](.\images\brokenlinks_2.png)



After execution the widget can be return no results (all links are correct) or widget can list artifacts where links are not correct. User can click on listed artifacts to select them in the module.

![](.\images\brokenlinks_3.png)

---------------------------------------

### Auto Number Sort Attribute

The widget is automatically updating integer values of specified sort attribute based on display order within the module or selected artefacts.



![Auto Number Sort Attribute](.\images\autonumbersortattribute_full.png)

**Version:**

Version 8.0 (release date 22.02.2022)

**Description:**
Widget updates integer values of specified sort attribute based on user settings and display order within a module or selected artifacts. Artifacts must be UNLOCKED before processing.

Sort Attribute: zAdm_Sort_SRS+, Start: 0, Incr: 1.

After starting the update, please WAIT UNTIL WIDGET COMPLETES PROCESSING before taking further action. Estimated time is 2 min/100 artifacts. If processing hangs, CHECK LOCKS.

**Use:**

Before you start working with the widget, you need to specify in its settings which attribute will store the integer value information, the initial value and by how much it will be incremented for each artifact.



![](.\images\autonumbersortattribute_1.png)



![](.\images\autonumbersortattribute_2.png)



The prepared widget works in 2 modes, for selected artifacts or for the whole module. 



**Simple Mode**

The user should select artifacts for which he wants to generate values and then click the "Update value for selected artifacts" button. The value of the previously selected attribute will be modified according to the rules set in the widget settings.



![](.\images\autonumbersortattribute_3.png)



**Module Mode**

The user opens the module in which he wants to generate values for the attribute and then clicks the "Update value for all artifacts in the open module." button. The value of the previously selected attribute will be modified according to the rules set in the widget settings.

**Important: Widget will only update selected attribute for artifact types that have selected attribute. Artefact types not containing the attribute will be just ignored.**



![](.\images\autonumbersortattribute_4.png)



**Error messages:**

In case of bad widget configuration (bad attribute name or attribute type). The widget will display an error message.




![](.\images\autonumbersortattribute_5.png)




---------------------------------------

### Compare

This extension allows to compare artifact primary text with other string based attribute.



![Compare](.\images\compare_full.png)



![Compare](.\images\compare_full_2.png)



**Version:**

Version 17 (release date 24.08.2020)



**Description:**

The widget was created to compare the primary text with the designated attribute. The comparison is done in two modes rich html text and plain text.

As the attributes in DOORS Next are stored in plain text, the widget offers 2 modes of operation to facilitate data analysis.

The data from the comparison is displayed in the widget, but there is also an additional page with more information that can be opened from within the widget.



**Use:**

The user in the first step must define the name of the attribute to be compared with the primary text. 



![](.\images\compare_1.png)



![](.\images\compare_2.png)



Once the user has defined an attribute to compare then they can move on to the main functionality of the widget. The user should select the artifact for which he wants to compare the primary text with the value of the attribute. The widget will return the information in a simplified manner. 



![](.\images\compare_3.png)



For more complex text, click on more information to see the information in a more manageable way.



![](.\images\compare_4.png)



---------------------------------------


### Cyber Security

Use widget to calculate values of Secure Risk Rating. Calculations are done for artifact type Threat Scenario.



![](.\images\cyberSecurity_full.png)

**Version:**

Version 1 (release date 07.4.2022)



**Description:**

Widget was create to calculate secure risk rating based on the provided inputs. Widget is only working with the data model that has correctly defined attributes and artifact types.

**IMPORTANT: The widget should be considered as an example that can be adapted to the business scenario defined by the customer.**



**Use:**

The widget has 2 modes of operation, it can calculate the Secure Risk Rating value for selected artifacts or for all artifacts in the module.

**Simple mode:**

For this mode, the user has to select the artifacts for which he wants to perform the calculation in DOORS Next and then click the "Calculate Secure Risk Rating for selected artifacts" button.



![](.\images\cyberSecurity_1.png)



**Module mode:**

The mode for modules is activated when the user opens a module and deactivated when the module is closed.

For an open module, the user can click the "Calculate Secure Risk Rating in module." button and the widget will calculate for all artifacts that have the appropriate attributes.



![](.\images\cyberSecurity_2.png)



![](.\images\cyberSecurity_3.png)




---------------------------------------

### Safety Risk Management

Use widget to calculate values of Probability of Occurrence of Harm and Risk. 



![](.\images\safetyRiskManagement_full.png)

**Version:**

Version 2 (release date 07.4.2022)



**Description:**

Widget was created to calculate values of Probability of Occurrence of Harm and Risk. Calculations are done for artifact type Initiating Cause, Hazards, Hazardous Situation and Harm.

Widget is only working with the data model that has correctly defined attributes and artifact types.

**IMPORTANT: The widget should be considered as an example that can be adapted to the business scenario defined by the customer.**



**Use:**

The widget has 2 modes of operation, it can calculate the Probability of Occurrence of Harm and Risk value for selected artifacts or for all artifacts in the module.

**Simple mode:**

For this mode, the user has to select the artifacts for which he wants to perform the calculation in DOORS Next and then click the "Calculate Probability of Occurrence of Harm and Risk for selected artifacts" button.



![](.\images\safetyRiskManagement_1.png)



**Module mode:**

The mode for modules is activated when the user opens a module and deactivated when the module is closed.

For an open module, the user can click the "Calculate Probability of Occurrence of Harm and Risk in module." button and the widget will calculate for all artifacts that have the appropriate attributes.



![](.\images\safetyRiskManagement_2.png)



![](.\images\safetyRiskManagement_3.png)




---------------------------------------

### Link Type Clone

The widget clones all selected link types into another link type. 



![Link Type Clone](.\images\linkTypeClone_full.png)

**Version:**

Version 1.1 (release date 17.11.2020)



**Description:**

The widget clones all selected link types into another link type. Extension is working similar way to IBM LinkSwapper but allows to select artifacts or set of artifacts for which the action will be performed.

The widget can be used to reverse sides of the links



**Important:**  Be aware that link types with out directions (side names) are treated as bidirectional.



**Use:**

First, the user must configure the widget to work with the appropriate link types and their directions. To do this, open the widget settings.



![](.\images\linkTypeClone_1.png)



![](.\images\linkTypeClone_2.png)



The user must correctly define URIs for the types of links he wants to change:

* Source Link Type (URI)
* Target Link Type (URI)*



Additionally, the direction of the link to be changed must be specified. Acceptable values are:

* In
* Out
* Bidirectional (for links without specified directions)



All information can be found in the data model configuration for the project in DOORS Next.



![](.\images\linkTypeClone_3.png)



![](.\images\linkTypeClone_4.png)



![](.\images\linkTypeClone_5.png)



**Below presented artifact before running the widget**



![](.\images\linkTypeClone_6.png)



To start widget the user should select artifacts for which links should be cloned to different link type or direction. After selecting artifacts user should click "Clone link types for selected artifacts" button.



![](.\images\linkTypeClone_0.png)



When process of cloning is done the widget will display status message with number of errors.



![](.\images\linkTypeClone_9.png)



**Below presented artifact after running the widget**



![](.\images\linkTypeClone_8.png)



---------------------------------------

### Link Type Delete

The widget deletes all links of selected link type. 



![](.\images\linkTypeDelete_full.png)

**Version:**

Version 1.1 (release date 17.11.2020)

**Description:**

The widget deletes all links of selected link type. After starting the process, please wait until the widget will finish the process before taking the next actions.

**Important:** Be aware that link types with out directions (side names) are treated as bidirectional.

We using the widget inside change set to avoid possibility of deleting wrong links in the artifacts.



**Use:**



First, the user must configure the widget to work with the appropriate link types and their directions. To do this, open the widget settings.



![](.\images\linkTypeDelete_1.png)



![](.\images\linkTypeDelete_2.png)



The user must correctly define URI for the type of link he/she wants to delete:

* Type of link to be deleted (URI)

  

Additionally, the direction of the link to be deleted must be specified. Acceptable values are:

* In
* Out
* Bidirectional (for links without specified directions)



All information can be found in the data model configuration for the project in DOORS Next.



![](.\images\linkTypeDelete_3.png)





![](.\images\linkTypeDelete_4.png)



![](.\images\linkTypeDelete_5.png)



**Below presented artifact before running the widget**



![](.\images\linkTypeDelete_6.png)



To start widget the user should select artifacts for which links should be deleted. After selecting artifacts user should click "Delete links for selected artifacts" button.



![](.\images\linkTypeDelete_7.png)



When process of deleting is done the widget will display status message with number of errors.



![](.\images\linkTypeDelete_8.png)



**Below presented artifact after running the widget**



![](.\images\linkTypeDelete_9.png)


---------------------------------------

### Style Clean

The widget is finding removing HTML styles from artifact content.



![Style Clean](.\images\styleClean_full.png)



**Version:**

Version 1.0 (release date 26.11.2020)

**Description:**

The widget is removing HTML styles from artifact content. The widget is  helpful in fixing issue with styles imported from Microsoft Word.

Additionally, the widget finds artifacts with nested styles.

HTML styles can be saved in DOORS Next when importing a Microsoft Word document or when copying content by copy and paste.

The widget removes the use of fonts, colors and all data stored by the styles attribute in HTML. The widget will not remove data changed by HTML tags such as ```<b> <i> <u> <strong>``` and others that define the font style.

**Use:**

The widget only functions in the module and does not work outside the open module, all buttons are locked if you are not in the module.



![](.\images\styleClean_1.png)



When the module is opened, the "Find all artifact with styles" button is activated. 



![](.\images\styleClean_2.png)



After pressing the "Find all artifact with styles" button, the widget will present list of all artifacts with nested styles. User can click each entry in the result log and artifact will be selected in the module.



![](.\images\styleClean_3.png)



When artifacts with nested styles are found, the "Clean HTML style for selected artifacts" button is activated.

**Important: Activating this button removes styles for all selected artifacts, not all found artifacts. **

If the user selects only one artifact, only that artifact will have the styles removed. 

If there are more than 200 artifacts found, the widget must be run multiple times. The limitation of DOORS Next is that a maximum of 200 artifacts can be selected at once.



When you run style removal, the widget will display information about the number of artifacts modified.



![](.\images\styleClean_4.png)




---------------------------------------

### Primary Text Copy To Attribute

Widget copies value from Primary Text (as plain text) to user-specified Destination Attribute.



![Primary Text Copy To Attribute](.\images\primaryTextCopy_full.png)



**Version:**

Version 10.0 (release date 15.01.2022)

**Description:**

Widget copies value from Primary Text (as plain text) to user-specified Destination Attribute (see Settings). If Destination Attribute does not exist for applicable artifact type, then that artifact is not updated.

Since primary text is rich-text and the attribute is plain-text, the copied text from primary text is converted to plain text. Please note that images, tables, hyperlinks, styles, among others, will not be copied into the selected attribute.

**Use:**

In the first step, the user should define the settings of the widget, in which he will specify the name of the attribute where the text is to be copied.

![](.\images\primaryTextCopy_1.png)

If the attribute does not exist, the main text will not be copied. This allows copying of primary text to begin for a list of artifacts, and for artifact types where there is no attribute, copying is simply ignored.

**Important: The attribute may not be assigned to the artifact type you selected in DOORS Next, but it must exist in the project in which you are making the change.**

Other settings available are a prefix that can be added before the copied text and the ability to add a space between the prefix and the copied text.



![](.\images\primaryTextCopy_2.png)



In the next step, select the artifacts for which you want to perform copy operations and press the "Copy Primary Text to Attribute" button.



![](.\images\primaryTextCopy_3.png)



When the operation is complete, the widget will display information for how many artifacts the data was copied for.



![](.\images\primaryTextCopy_4.png)



---------------------------------------

### Module Fill



The Module Fill widget is creating a module structure based on the pairs of artfact identifiers and level in module structure. 

**Version:**

Version 1 (release date 24.2.2021)



**Description:**

The Module Fill widget is creating a module structure  based on the pairs of artfact identifiers and level in module structure. Requirements should be already in level order before starting the  widget as the level is used only to define depth in the module.



![Module Fill](.\images\moduleFill_full.png)



**Example of data input:**

`[231,1][3513,1.1][67452,1.1.1][65,2][456,3]`

**Important: The nesting level of the requirements is defined not by the numbers e.g. 1.1, 1.2.2. but by the number of dots between them, i.e. 1.1 is level 1, 1 is level 0, 1.1.1.1 is level 3.**



The widget is applicable when building modules for a large number of artifacts that have been imported into DOORS Next from formats such as Microsoft Excel or other databases, for example.

The widget is run in 2 steps, the first step is to search the project for artifacts to build a module from. This process can take a long time if the number of artifacts is significant. In this step, it is also checked if the structure of the entered data is correct.

The widget only works in an open module, outside the module the buttons will be inactive.



**Use:**

Before using the widget, you must define in its settings what type of artifact is Heading. This setting is required for all artifacts of type Heading to be stored correctly in the module.



![](.\images\moduleFill_0.png)



![](.\images\moduleFill_2.png)



In the next step, open the module to activate the full functionality of the widget.

![](.\images\moduleFill_3.png)



When you activate the widget, you need to provide a list of artifacts and the levels at which they should be placed in the module.  

***For example:*** `[9234,1][15677,1.1]`

When the list of artifacts is ready, click the "Prepare the data" button and the widget will check if the artifacts exist in the project and if the structure of the given data is true.

![](.\images\moduleFill_4.png)



This process can take several minutes, depending on the amount of data provided.

When you receive a message that the data has been prepared correctly, the "Fill aa module" button will be unlocked.

![](.\images\moduleFill_5.png)

After pressing the "Fill a module", the module will be filled with artifacts in a user defined structure.

Reload the web page to see the correctly entered data.

![](.\images\moduleFill_6.png)




---------------------------------------

### Quality Assurance 

This extension allows you to check quality of selected artifacts based on the defined rules.



![Quality Assurance](.\images\qualityAssurance_full.png)



**Version:**

Version 3.0 (release date 25.05.2022)



**Description:**

The widget was created to facilitate the daily work of an engineer or requirements manager.

It allows you to check a list of selected requirements, whole module or collection of requirements against selected rules.

The widget has been created in such a way that it can be easily extended with new rules by using JavaScript.

At the moment, there are implemented such rules as.

* Embedded artifacts in the primary text - search for all artifacts with other artifacts embedded in them
* Embedded artifacts in the primary text and embedded link - finding all artifacts with embedded artifacts, where no link to the embedded artifact has been created.

The list of rules can of course be extended and modified. Examples of proposed rules:

* The definition must contain primary text
* Artifact of certain type must have certain attributes filled in
* Artefacts must have certain relationships to each other.

The widget should be treated as a ready-made framework for requirements assessment, which can be extended.



**Use:**

First, specify in the widget the list of rules that should be checked. The list can always be extended with new rules using JavaScript. Currently, in the widget, all rules are grouped for all artifact types, you can define rules specific to selected artifact types.

![](.\images\qualityAssurance_1.png)

When you hover over each rule, a tooltip about the rule itself appears. The user's choice of rules is saved automatically in the settings of the widget itself.



The widget works in two modes: for user selected artifacts and for entire modules. 

**Simple Mode:**

In this mode, select the artifacts which you want to do checks and click the "Verify selected artefacts" button.

![](.\images\qualityAssurance_2.png)

**Module Mode:**

In this mode, the widget goes through all artifacts in the module (or collection) to do checks. Option is only activated when module is open.

After opening the module please click "Verify all artefacts in the module of collection" button to do checks.



![](.\images\qualityAssurance_3.png)



Running the widget in both modes generates results in the same way as a list in which you can click through the results which further highlights artifacts in DOORS Next.

Each result, when hovering the mouse over it, displays which rules are not satisfied. Additionally it is possible to mark all found results and tag them with tags.



![](.\images\qualityAssurance_4.png)

The results can also be exported as a Microsoft Excel file.



![](.\images\qualityAssurance_5.png)



---------------------------------------


### D&R Rule Checker

Widget checks completeness of attributes and D&R rule compliance for selected artifacts (i.e., requirements, parameters, and/or terms). 

**An example of adapting the Quality Assurance Widget for a customer data model.**



![D&R Rule Checker](.\images\genericValidation_full.png)




**Version:**

Version 10.0g (release date 17.05.2022)

**Description:**

The widget was created to facilitate the daily work of an engineer or requirements manager.

It allows you to check a list of selected requirements, whole module or collection of requirements against selected rules.

The widget has been created in such a way that it can be easily extended with new rules by using JavaScript and should be treated as a ready-made framework for requirements assessment, which can be extended.



**D&R Rule checker is an example of adapting the Quality Assurance Widget for a customer data model.**



**Use:**

First, specify in the widget the list of rules that should be checked. The list can always be extended with new rules using JavaScript. 

![](.\images\genericValidation_1.png)



The user's choice of rules is saved automatically in the settings of the widget itself. The widget works in two modes: for user selected artifacts and for entire modules. 

**Simple Mode:**

In this mode, select the artifacts which you want to do checks and click the "Verify selected artefacts" button.

![](.\images\genericValidation_2.png)

**Module Mode:**

In this mode, the widget goes through all artifacts in the module (or collection) to do checks. Option is only activated when module is open.

After opening the module please click "Verify all artefacts in the module of collection" button to do checks.



![](.\images\genericValidation_3.png)



Running the widget in both modes generates results in the same way as a list in which you can click through the results which further highlights artifacts in DOORS Next.

![](.\images\genericValidation_4.png)



The results can also be exported as a Microsoft Excel file.



![](.\images\genericValidation_5.png)



---------------------------------------

### IBM Import Repair

This extension allows you to take module with a flat hierarchy but an attribute on the artifacts that indicates depth and move all the artifacts in it to the corresponding depth.



![IBM Import Repair](.\images\importrepair_full.png)

**Version:**

IBM release for 7.0.2

**Description:**

This extension allows you to take module with a flat hierarchy but an attribute on the artifacts that indicates depth and move all the artifacts in it to the corresponding depth.

**Use:**

No documentation provided by IBM.

---------------------------------------


### IBM Attributes and Links

This extension shows the attributes and links of the artifact that is currently selected.

![](.\images\attributes_full.png)



**Version:**

IBM release for 7.0.2

**Description:**

This extension shows the attributes and links of the artifact that is currently selected.

**Use:**

No documentation provided by IBM.

---------------------------------------

### IBM ASIL Guidance

This extension checks modules that include information related to the ISO 26262 ASIL standard.



![ASIL Guidance](C:\Users\Bartosz Chrabski\Desktop\Readme\images\asilguidance_full.png)

**Version:**

IBM release for 7.0.2

**Description:**

This extension checks modules that include information related to the ISO 26262 ASIL standard. Use this extension on a module that contains artifacts that have these attributes: Controllability Class, Probability Class, Severity Class, and ASIL. The extension computes the correct value for the ASIL attribute based on the Controllability, Probability, and Severity values. You can run the extension on a module and produce a report that shows which module rows have incorrect ASIL values. You can then correct any incorrect values.

**Use:**

No documentation provided by IBM.

---------------------------------------

### IBM Module Explorer

This extension shows a hierarchical tree view of the module, which you can click to explore.




![IBM Module Explorer](.\images\moduleexplorer_full.png)

**Version:**

IBM release for 7.0.2

**Description:**

This extension shows a hierarchical tree view of the module, which you can click to explore. You can search for artifacts, collapse or expand the hierarchy to a particular level, and toggle to show only headings or all of the artifacts in the module.

**Use:**

No documentation provided by IBM.

---------------------------------------

### IBM Split and Join

This extension operates on Text artifacts in a Module and and either  join a selected group of them into a single artifact or take a single  artifact with multiple paragraphs and split them out into individual  artifacts in the Module.



![IBM Split and Join](.\images\splitandjoin_full.png)

**Version:**

IBM release for 7.0.2

**Description:**

This extension operates on Text artifacts in a Module and and either  join a selected group of them into a single artifact or take a single  artifact with multiple paragraphs and split them out into individual  artifacts in the Module.

**Use:**

No documentation provided by IBM.

---------------------------------------
## Frequently Asked Questions (FAQ)
## Contact

Bartosz Chrabski

<info@reqpro.com>

<info@smarterprocess.pl>